-- Middleclass library.
local class = require( ( _G.scrappyDir or "" ) .. "inventory.middleclass" )

--- Required libraries.
local json = require( "json" )
local mime
if system.getInfo( "platform" ) == "html5" or system.getInfo( "platform" ) == "nx64" then
	local base64 = require( ( _G.scrappyDir or "" ) .. "data.base64" )
	mime =
	{
		b64 = base64.encode,
		unb64 = base64.decode
	}
else
	mime = require( "mime" )
end

-- Localised functions.
local time = os.time
local encode = json.encode
local decode = json.decode
local pathForFile = system.pathForFile
local open = io.open
local close = io.close
local b64 = mime.b64
local unb64 = mime.unb64

-- Localised values.
local DocumentsDirectory = system.DocumentsDirectory

--- Class creation.
local Inventory = class( "Inventory" )

--- Initiates a new Inventory object.
-- @param name The name of the new inventory.
-- @return The new inventory.
function Inventory:initialize( name )

	-- New object
	local inventory = {}

	-- Set the name of this inventory
	self._name = name

	-- Filename for the inventory on disk
	self._filename = self._name .. ".scrappyInventory"

	-- Table to store held items
	self._items = {}

end

--- Initiates this inventory.
-- @param params The params for the new inventory.
-- @return The inventory.
function Inventory:init( params )

	-- Get the params, if any
	self._params = params or {}

	self:load()

end

--- Picks up an item.
-- @param id The id of the item.
-- @param count The number to pickup. Optional, defaults to 1.
function Inventory:pickupItem( id, count )

	self._items[ id ] = self._items[ id ] or
	{
		time = time()
	}

	self._items[ id ].count = ( self._items[ id ].count or 0 ) + ( count or 1 )

end

--- Drops an item.
-- @param id The id of the item.
-- @param count The number to drop. Optional, defaults to 1.
-- @param reset Should the fact the item has been picked up in the past be reset? Optional, defaults to false.
function Inventory:dropItem( id, count, reset )

	if self._items[ id ] and self._items[ id ].count > 0 then
		self._items[ id ].count = ( self._items[ id ].count or 0 ) - ( count or 1 )
	end

	if self._items[ id ] and self._items[ id ].count < 0 then
		self._items[ id ].count = 0
	end

	if reset then
		self._items[ id ] = nil
	end

end

--- Checks if an item has been picked up. Not necessarily being held as the count could still be 0.
-- @param id The id of the item.
-- @return True if it has been, false otherwise.
function Inventory:hasItemBeenPickedUp( id )
	return self._items[ id ] ~= nil
end

--- Checks if an item is currently held. i.e. at least 1 of them.
-- @param id The id of the item.
-- @return True if it is, false otherwise.
function Inventory:isItemCurrentlyHeld( id )
	return self:countItem( id ) > 0
end

--- Counts how many instances of an item is currently held.
-- @param id The id of the item.
-- @return The item count.
function Inventory:countItem( id )
	return self._items[ id ] and self._items[ id ].count or 0
end

--- Counts how many instances of a all items are currently held.
-- @return The count.
function Inventory:countItems()

	-- Set the count to zero
	local count = 0

	-- Loop through all items
	for k, v in pairs( self:getItems() ) do

		-- Counting the item
		count = count + self:countItem( k )

	end

	-- And return the total count
	return count

end

--- Gets the currently held items.
-- @param asList True if you want them returned as an indexed list. Optional, defaults to false.
-- @return The item list.
function Inventory:getItems( asList )

	-- Should this be an indexed list?
	if asList then

		-- Create the empty list
		local items = {}

		-- Loop through all the items
		for k, v in pairs( self:getItems() ) do

			-- Do we actually still have this item?
			if self:isItemCurrentlyHeld( k ) then

				-- Then add it
				items[ #items + 1 ] = k

			end
		end

		-- And return them
		return items

	else -- Otherwise

		-- Create an empty list
		local items = {}

		-- Loop through the items
		for k, v in pairs( self._items ) do

			-- Check if we're holding it
			if self:isItemCurrentlyHeld( k ) then

				-- And add it
				items[ k ] = v

			end

		end

		-- Just return them as is
		return items

	end

end

--- Resets this inventory.
function Inventory:reset()
	self._items = {}
end

--- Exports this inventory.
-- @return The exported data.
function Inventory:export()
	return self:isEncodingDisabled() and encode( self._items ) or b64( encode( self._items ) )
end

--- Imports previously exported inventory data.
-- @param data The data to import.
-- @return True if it was successful, false otherwise.
function Inventory:import( items )

	-- Try to decode the items
	items = type( items ) == "string" and decode( self:isEncodingDisabled() and items or unb64( items ) ) or items

	-- Did we fail to decode the items yet encoding isn't marked as disabled?
	if type( items ) == "string" and not self:isEncodingDisabled() then

		-- Try to do a regular decode
		items = decode( items )

		-- Did it work?
		if type( items ) == "table" then

			-- Then mark encoding as disabled
			self:disableEncoding()

		end

	end

	-- Did we decode the items?
	if items and type( items ) == "table" then

		-- Store out the items
		self._items = items

		-- Return as successful
		return true

	else

		-- Ensure we have atleast a blank table
		self._items = self._items or {}

		-- Return as failed
		return false

	end


end

--- Saves this inventory.
function Inventory:save()

	-- Get the inventory file on disk
	local file = open( pathForFile( self._filename, DocumentsDirectory ), "w" )

	-- Do we have a file?
	if file then

		-- Export the data
		local data = self:export()

		-- Write it out
		file:write( data )

		-- Close the file
		close( file )

		-- Nil out the file handle
		file = nil

		-- Return true as it was saved
		return true

	end

	-- Return false as it wasn't saved
	return false

end

--- Loads this inventory.
function Inventory:load()

	-- Get the path for the file
	local path = pathForFile( self._filename, DocumentsDirectory )

	-- Do we have a path?
	if path then

		-- Load the file
		local file = open( path, "r" )

		-- Do we have a file?
		if file then

			-- Clear the items table
			self._items = {}

			-- Read the data
			local data = file:read( "*a" ) or ""

			-- Close the file
			close( file )

			-- Nil out the file handle
			file = nil

			-- And import the data
			self:import( data )

			-- Mark us as loaded
			self._loaded = true

		end

	end

end

--- Checks if the inventory is loaded.
-- @return True if it is, false otherwise.
function Inventory:isLoaded()
	return self._loaded
end

--- Disabled binary encoding when saving/loading.
function Inventory:disableEncoding()
	self._encodingDisabled = true
	self:save()
end

--- Eanbles binary encoding when saving/loading.
function Inventory:enableEncoding()
	self._encodingDisabled = false
	self:save()
end

--- Checks if binary encoding is currently disabled.
-- @return True if it is, false otherwise.
function Inventory:isEncodingDisabled()
	return self._encodingDisabled
end

--- Destroys this inventory.
function Inventory:destroy()

end

-- Return the class
return Inventory
