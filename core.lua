--- Required libraries.
local Inventory = require( ( _G.scrappyDir or "" ) .. "inventory.inventory" )

-- Localised functions.

-- Localised values

--- Class creation.
local library = {}

library._inventories = {}
library._primary = nil

--- Loads an inventory.
-- @param name The name of the inventory.
-- @return The new inventory.
function library:loadInventory( name )

	-- Do we have an inventory with this name?
	if self._inventories[ name ]  then

		-- If so, return it
		return self._inventories[ name ]

	-- Otherwise...
	else

		-- Create the inventory
		local inventory = Inventory:new( name )

		-- Store it out
		self._inventories[ name ] = inventory

		-- And return it
		return inventory

	end

end

--- Gets a loaded inventory.
-- @param name The name of the inventory. Optional, defaults to the default scrappy one.
-- @return The inventory.
function library:getInventory( name )
	return name and self._inventories[ name ] or self._primary
end

--- Checks if an inventory exists.
-- @param name The name of the inventory.
-- @return True if it does, false otherwise.
function library:inventoryExists( name )
	return name and self._inventories[ name ] ~= nil
end

-- If we don't have a global Scrappy object i.e. this is the first Scrappy plugin to be included
if not Scrappy then

	-- Create one
	Scrappy = {}

end

-- If we don't have a Scrappy Inventory library
if not Scrappy.Inventory then

	-- Then store the library out
	Scrappy.Inventory = library

end

-- Do we not have our primary inventory?
if not library._primary then

	-- Create the primary inventory and store it out
	library._primary = library:loadInventory( "scrappy" )

end

-- Create all the passthrough functions for the primary inventory
function library:init( ... ) return library._primary:init( ... ) end
function library:pickupItem( ... ) return library._primary:pickupItem( ... ) end
function library:dropItem( ... ) return library._primary:dropItem( ... ) end
function library:hasItemBeenPickedUp( ... ) return library._primary:hasItemBeenPickedUp( ... ) end
function library:isItemCurrentlyHeld( ... ) return library._primary:isItemCurrentlyHeld( ... ) end
function library:countItem( ... ) return library._primary:countItem( ... ) end
function library:countItems( ... ) return library._primary:countItems( ... ) end
function library:getItems( ... ) return library._primary:getItems( ... ) end
function library:export( ... ) return library._primary:export( ... ) end
function library:import( ... ) return library._primary:import( ... ) end
function library:save( ... ) return library._primary:save( ... ) end
function library:load( ... ) return library._primary:load( ... ) end
function library:reset( ... ) return library._primary:reset( ... ) end
function library:destroy( ... ) return library._primary:destroy( ... ) end

-- Return the new library
return library
